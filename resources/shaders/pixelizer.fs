#version 330

// Input vertex attributes (from vertex shader)
in vec2 fragTexCoord;
in vec4 fragColor;

// Input uniform values
uniform sampler2D texture0;
uniform vec4 colDiffuse;

// NOTE: Add here your custom variables

// NOTE: Render size values must be passed from code
uniform float renderWidth;
uniform float renderHeight;

uniform float pixelSize;

uniform float whiteFactor;

// Output fragment color
out vec4 finalColor;

void main()
{
    float dx = pixelSize*(1.0/renderWidth);
    float dy = pixelSize*(1.0/renderHeight);

    vec2 coord = vec2(dx*floor(fragTexCoord.x/dx), dy*floor(fragTexCoord.y/dy));

    // Pixelize effect as vector3
    vec3 tc = texture(texture0, coord).rgb;

    // Fade effect using whiteFactor as color's alpha channel
    finalColor = vec4(tc, whiteFactor);
}