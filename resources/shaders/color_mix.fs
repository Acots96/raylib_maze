#version 330

// Input vertex attributes (from vertex shader)
in vec3 vertexPos;
in vec2 fragTexCoord;
in vec4 fragColor;

#define MAX_COLORS 3

// Input uniform values
uniform vec4 color0;
uniform vec4 color1;

uniform float dividerTop;
uniform float dividerBottom;

uniform float direction;

// Output fragment color
out vec4 finalColor;

void main()
{
    // Vertical coordinate in texture (fractional part only)
    float y = fract(fragTexCoord.s);
    
    // Above dividerTop -> 0.0 (not needed)
    // Between dividerTop and dividerBottom -> interval (0.0,1.0) (needed)
    // Under dividerBottom -> 1.0 (not needed)
    float step = smoothstep(dividerTop, dividerBottom, y);

    // We only want to draw between dividerTop and dividerBottom, so we get the
    // fractional part because if the coordinate is above dividerTop (0.0) or
    // under dividerBottom (1.0) the fractional part will be 0, while if it is
    // in between both dividers it will be 0.X
    float final0 = fract(step);
    // Inverted value. We need both values for both directions up and down
    float final1 = fract(1 - step);

    // Use one fractional part or the other depending on the direction
    step = mix(final1, final0, direction);
    finalColor = mix(color0, color1, step);
}