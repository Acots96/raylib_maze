#pragma once
#include "Actor.h"
#include <raylib.h>

// Represents a prop in a map
class Prop : public Actor
{
	private:
		float _scale = 1.0f;
		Model _model;
		BoundingBox _body;
		float _rotateSpeed = 30.0f;
		float _axisYAngle = 0.0f;
		//
		Shader _defaultShader;

	public:
		void Update() override;
		void Draw() override;
		BoundingBox GetBody();
		void SetScale(float scale);
		void SetModel(const Model& model);
		void SetShader(Shader);
		void ResetShader();
		// Computes the body (box) using the default scale
		void ComputeBody();
		// Computes the body (box) using the given scale
		void ComputeBody(float scale);
		// Computes the body (box) using the given box
		void ComputeBody(const BoundingBox& body);
		~Prop() override;
};
