#include "Player.h"
#include <iostream>
#include "RaylibUtils.h"
#include <string>
#include "GameManager.h"
#include "ResourcesManager.h"

using namespace std;

Player::Player(GameManager* manager, MazeMap* map, Vector3 startPosition, float bodyScale) : Actor()
{
	_manager = manager;
	_bodyScale = bodyScale;
	Reset(map, startPosition);

	ShowCursor();

	// true to ignore walls colliders
	_walkThroughWallsCheat = false;
}

void Player::Reset(MazeMap* map, Vector3 startPosition)
{
	_map = map;

	_position = startPosition;
	Translate(_position);
	_startY = _position.y;
	Vector3 fwd = -forward3;
	_rotation = { 0, asinf(fwd.z) * RAD2DEG , 0 };
	fwd = {
		cosf(_rotation.y * DEG2RAD),
		-sinf(_rotation.x * DEG2RAD),
		sinf(_rotation.y * DEG2RAD)
	};
	_lookAtPosition = _position + fwd;

	_turnSpeed = 30.0f;
	_moveSpeed = 1.0f; // m/s
	_runMultiplier = 2.5f;
	_stepAmplitude = 0;
	_canStepSound = false;
	_stepFoot = -0.8f;

	// *I know I could have used the already made system for first person*
	// *camera, but I wanted to implement my own, though it is not finished*
	// *because I wanted to implemente it in Actor class (position and rotation)*
	_camera = Camera3D();
	_camera.position = _position;
	_camera.target = _lookAtPosition;
	_camera.up = { 0.0f, 1.0f, 0.0f };
	_camera.fovy = 60.0f;

	_body = BoundingBox();
	_body.min = _position - one3 * _bodyScale / 2;
	_body.max = _position + one3 * _bodyScale / 2;
}

void Player::Update()
{
	UpdatePosition();
	UpdateRotation();

	_camera.position = _position;
	_camera.target = _lookAtPosition;

	if (Prop* p; (p = _map->IsTouchingProp(_body)) != nullptr)
	{
		_manager->CollectProp(p);
	}
	else _manager->CheckFinishMaze(_body);

	// Check for XVision input
	if (IsKeyPressed(KEY_V)) _manager->ActivateXVision(true);
}

void Player::UpdatePosition()
{
	// Check the input
	Vector3 input = { 0.0f, 0.0f, 0.0f };
	if      (IsKeyDown(KEY_W)) input.z = 1.0f;
	else if (IsKeyDown(KEY_S)) input.z = -1.0f;
	if      (IsKeyDown(KEY_A)) input.x = -1.0f;
	else if (IsKeyDown(KEY_D)) input.x = 1.0f;

	if (input != zero3)
	{
		// Check if the player is running
		bool isShift = IsKeyDown(KEY_LEFT_SHIFT);
		float r = isShift ? _runMultiplier : 1.0f;

		// Rotation matrix
		float a = _rotation.y * DEG2RAD;
		Matrix rotY = {
			-sinf(a), 0, cosf(a), 0,
			0, 1, 0, 0,
			cosf(a), 0, sinf(a), 0,
			0, 0, 0, 1,
		};

		// Use the rotation matrix to compute player's rotation
		Vector3 local = input;
		Vector3 world = rotY * local;
		if (input.x != 0 || input.z != 0)
		{
			// Use of sinus function to simulate the steps movement
			input = Vector3Normalize(input);
			float speedMult = 0.225f;
			_stepAmplitude += (isShift ? 1.5f : 1.0f) * speedMult;
			float amplitudeYMult = isShift ? 0.0325f : 0.02f;
			world.y = _startY + sinf(_stepAmplitude) * amplitudeYMult;
		}

		Vector3 dir = Vector3Normalize({ world.x, 0, world.z });

		// Calculate player's body on next frame
		BoundingBox b = _body;
		Vector3 offset = one3 * _bodyScale / 2;
		Vector3 next = _position + dir * _moveSpeed * r * GetFrameTime();
		b.min = next - offset;
		b.max = next + offset;

		// If player's body in next frame is not colliding with any obstacle,
		// then, the actual body can be updated with the future one
		RayCollision collision{};
		if (_walkThroughWallsCheat || !_map->IsColliding(b, collision))
		{
			_position = next;
			_position.y = world.y;
			Translate(_position);
			_body.min = _position - offset;
			_body.max = _position + offset;

			if (_canStepSound && cosf(_stepAmplitude) > 0)
			{
				float min = 0.95f + _stepFoot;
				float max = 1.05f + _stepFoot;
				float pitch = min + ((float)rand() / (float)RAND_MAX) * (max - min);
				SetSoundPitch(ResourcesManager::StepSound, pitch);
				PlaySound(ResourcesManager::StepSound);
				_canStepSound = false;
				_stepFoot *= -1;
			}
			else if (cosf(_stepAmplitude) < 0)
				_canStepSound = true;
		}

		// *When the body collides with any obstacle it stops,*
		// *and I wanted to calculate some movement depending on the*
		// *movement direction and the normal of the obstacle, but I 
		// *need more time...*
		//else
		//{
		//	a = 180 - acosf(Vector3DotProduct(dir, collision.normal)) * RAD2DEG;
		//	if (a != 0)
		//	{
		//		Vector3 cross = Vector3CrossProduct(dir, collision.normal);
		//		float dot = Vector3DotProduct(up3, cross);
		//		if (dot > 0)
		//			a *= -1;
		//		float pct = a / 90;
		//		dir = Vector3CrossProduct(up3, collision.normal) * pct;
		//		Vector3 next = _position + dir * _moveSpeed * r * GetFrameTime();
		//		//
		//		_position = next;
		//		_position.y = world.y;
		//		Translate(_position);
		//		_body.min = _position - offset;
		//		_body.max = _position + offset;
		//	}
		//}
	}
}

void Player::UpdateRotation()
{
	// Get delta's mouse movement and calculate the rotation
	Vector2 delta = GetMouseDelta();

	if (delta.x != 0 || delta.y != 0)
	{
		// Y axis rotates with mouse X movement
		_rotation.y += delta.x * _turnSpeed * GetFrameTime();
		// X axis rotates with mouse Y movement with min and max limits to avoid gimbal lock
		_rotation.x = fmaxf(-80.0f, fminf(80.0f, _rotation.x + delta.y * _turnSpeed * GetFrameTime()));
	}

	Vector3 fwd = {
		cosf(_rotation.y * DEG2RAD),
		-sinf(_rotation.x * DEG2RAD),
		sinf(_rotation.y * DEG2RAD)
	};

	_lookAtPosition = _position + fwd;
}

void Player::Draw()
{
	
}

Camera3D Player::GetCamera()
{
	return _camera;
}