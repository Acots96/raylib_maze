#include "GameManager.h"
#include "ResourcesManager.h"
#include <iostream>
#include "Player.h"

GameManager::GameManager() : Actor()
{
	// Given the Cubicmaps, fills the _maps list with MazeMap objects

	for (auto const m : ResourcesManager::Cubicmaps)
	{
		//_maps.push_back(new MazeMap(m, {0.0f, 0.0f, 0.0f}, 1.0f));
		_cubicmaps.push_back(m);
	}
	_currentMapIdx = 0;
	_currentMap = new MazeMap(_cubicmaps[_currentMapIdx], { 0.0f, 0.0f, 0.0f }, 1.0f);

	// Player

	_player = new Player(this, _currentMap, _currentMap->GetStartPosition(), 0.5f);

	// Player's camera render

	_fullViewport = { 0.0f, 0.0f, (float)GetRenderWidth(), (float)GetRenderHeight() };
	_fullViewPosition = { 0.0, 0.0f };
	_fullTexture = LoadRenderTexture(_fullViewport.width, _fullViewport.height);
	_fullViewport.height *= -1;

	// XVision's camera render

	float topAspectRatio = 1.5f;
	float h = GetRenderHeight() * 0.1f;
	_xViewport = { 0.0f, 0.0f, (float)GetRenderWidth(), (float)GetRenderHeight() };
	_xViewPosition = { 0.0, 0.0f };
	_xTexture = LoadRenderTexture(_xViewport.width, _xViewport.height);
	_xViewport.height *= -1;
	_xCamera = Camera3D();
	_xCamera.projection = CAMERA_PERSPECTIVE;
	_xCamera.position = { 0, 0, 2 };
	_xCamera.up = { 0, 1, 0 };
	_xCamera.fovy = 60.0f;

	_collectedProps = {};

	// Start/finish fade effect, a mix of pixel shader and grayscale shader
	// applied to the player's camera render texture, so it looks like a
	// post-processing effect.
	// - For this effect, a simple value for each effect is increased/decreased
	//   during X time depending on whether it is a fadeout transition (start maze: 
	//   pixelSize from max to 0, whiteFactor from 0 to 1) or a fadein transition
	//   (finish maze: pixelsize from 0 to max and whiteFactor from 1 to 0).
	// (further details in resources/shaders/ pixelizer.fs)

	_fadeShader = ResourcesManager::FadeShader;
	_pixelSizeLoc = GetShaderLocation(_fadeShader, "pixelSize");
	_whiteFactorLoc = GetShaderLocation(_fadeShader, "whiteFactor");
	Vector2 s { GetRenderWidth(), GetRenderHeight() };
	SetShaderValue(_fadeShader, GetShaderLocation(_fadeShader, "renderWidth"), &s.x, SHADER_UNIFORM_FLOAT);
	SetShaderValue(_fadeShader, GetShaderLocation(_fadeShader, "renderHeight"), &s.y, SHADER_UNIFORM_FLOAT);
	//
	_transition = FadeOut;
	_transitionTime = 2.0f;
	_transitionPct = 0;
	PlaySound(ResourcesManager::StartMazeSound);

	// Exit door (finish maze) effect, consisting of a "laser" moving up
	// and down along a transparent cylinder.
	// - For this effect, there are 2 values that simulate the top and bottom
	//   of a subpart of the main cylinder, both being updated each frame towards
	//   up or down to simulate the vertical movement (same direction and same
	//   distance between both values).
	// (further details in resources/shaders/ color_mix.fs)

	Vector4 blank = { 0.0f, 0.0f, 0.0f, 0.0f };
	Vector4 laser = { 0.4f, 1.0f, 0.4f, 0.8f };
	_dividerTop = -1.0f;
	_dividerBottom = 0.0f;
	_topBottomValue = 0;
	_direction = 1;
	//
	_finishDoorShader = ResourcesManager::FinishDoorShader;
	_directionLoc = GetShaderLocation(_finishDoorShader, "direction");
	_dividerTopLoc = GetShaderLocation(_finishDoorShader, "dividerTop");
	_dividerBottomLoc = GetShaderLocation(_finishDoorShader, "dividerBottom");
	SetShaderValue(_finishDoorShader, _directionLoc, &_direction, SHADER_UNIFORM_FLOAT);
	SetShaderValue(_finishDoorShader, GetShaderLocation(_finishDoorShader, "color0"), &blank, SHADER_UNIFORM_VEC4);
	SetShaderValue(_finishDoorShader, GetShaderLocation(_finishDoorShader, "color1"), &laser, SHADER_UNIFORM_VEC4);
	//
	Image img = GenImageColor(512, 512, WHITE);
	Texture tex = LoadTextureFromImage(img);
	UnloadImage(img);
	_finishDoor = LoadModelFromMesh(GenMeshCylinder(0.25f, 1.0f, 16));
	_finishDoor.materials[0].shader = _finishDoorShader;
	_finishDoor.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = tex;
	//
	Vector3 f = _currentMap->GetFinishPosition();
	float y = _currentMap->GetScale() / 2.0f;
	_finishDoorBody = BoundingBox();
	_finishDoorBody.min = _finishDoorBody.max = f;
	_finishDoorBody.min.y -= y;
	_finishDoorBody.min.y += y;

	// true to show the exit door with XVision
	_showExitDoorCheat = false;

	// XVision effect, to allow the player to see through walls the remaining
	// items marked in red.
	// - For this effect, there is a second camera that only renders the props and a redscale
	//   shader which is applied to all of them so they'll appear in the screen painted in red.
	//   Moreover, the grayscale shader is applied to the main render texture so it will be
	//   gray, to simulate a more "realistic" XVision effect.
	// (further details in resources/shaders/ grayscale.fs and redscale.fs)

	// true to remove the cooldown time
	_superXVisionCheat = false;

	_xVisionTime = 2.0f;
	_xVisionDeltaTime = 0;
	_xVisionCooldownTime = _superXVisionCheat ? 0.1f : 5.0f;
	_xVisionCooldownDeltaTime = _xVisionCooldownTime;
	//
	_xVisionImageShader = ResourcesManager::XVisionImageShader;
	Vector4 v = { 0.5f, 0.5f, 0.5f, 1.0f };
	SetShaderValue(_xVisionImageShader, GetShaderLocation(_xVisionImageShader, "colDiffuse"), &v, SHADER_UNIFORM_VEC4);
	//
	_xVisionObjectShader = ResourcesManager::XVisionObjectShader;

	//

	_state = Playing;
}

void GameManager::Update()
{
	if (_transition != NoEffect)
	{
		ComputeTransition();
	}	
	else
	{
		_player->Update();
		_currentMap->Update();

		// Check if the XVision effect is active and update it
		if (_xVisionDeltaTime > 0)
		{
			_xVisionDeltaTime -= GetFrameTime();
			auto cam = _player->GetCamera();
			_xCamera.position = cam.position;
			_xCamera.target = cam.target;

			UpdateMusicStream(ResourcesManager::XVisionSound);

			if (_xVisionDeltaTime <= 0) ActivateXVision(false);
		}
		// Otherwise, check if it is "cooling down" and update it
		else if (_xVisionCooldownDeltaTime > 0)
			_xVisionCooldownDeltaTime -= GetFrameTime();

		UpdateMusicStream(ResourcesManager::MazeTheme);
	}
}

void GameManager::Draw()
{
	// Loading screen
	if (_transition == Loading)
	{
		float w = GetRenderWidth();
		float h = GetRenderHeight();

		BeginDrawing();
		ClearBackground(BLACK);
		float font = w * 0.05f;
		int tw = MeasureText("Loading...", font);
		DrawText("Loading...", w * 0.5f - tw, h * 0.5f, font, WHITE);
		EndDrawing();
	}
	// Maze
	else
	{
		// Draw the player's camera
		BeginTextureMode(_fullTexture);
		ClearBackground(BLACK);
		BeginMode3D(_player->GetCamera());

		_player->Draw();
		_currentMap->Draw();
		if (_transition == NoEffect && _currentMap->GetProps().empty())
			DrawFinishDoor();

		EndMode3D();
		EndTextureMode();

		//

		// Draw XVision's camera if activated (only props)
		bool xTexture = _xVisionDeltaTime > 0;
		// XVision render texture
		if (xTexture)
		{
			BeginTextureMode(_xTexture);
			ClearBackground({ 0, 0, 0, 50 });
			BeginMode3D(_xCamera);
			for (auto& p : _currentMap->GetProps()) p->Draw();
			if (_showExitDoorCheat && _currentMap->GetProps().empty()) DrawFinishDoor();
			EndMode3D();
			EndTextureMode();
		}

		//

		// Draw the main render texture and, if activated, XVision's render texture
		BeginDrawing();
		ClearBackground(BLACK);

		// Is Transition on? -> fade shader
		if (_transition != NoEffect)
			BeginShaderMode(_fadeShader);

		if (xTexture)
		{
			// Draw the main textrure with grayscale shader
			BeginShaderMode(_xVisionImageShader);
			DrawTextureRec(_fullTexture.texture, _fullViewport, _fullViewPosition, WHITE);
			EndShaderMode();
			// Draw the texture with the props in red
			DrawTextureRec(_xTexture.texture, _xViewport, _xViewPosition, WHITE);
		}
		else
		{
			// Main render texture
			DrawTextureRec(_fullTexture.texture, _fullViewport, _fullViewPosition, WHITE);
		}

		// UI
		if (_transition == NoEffect)
			DrawUI();
		else if (_transition != NoEffect)
			EndShaderMode();

		EndDrawing();
	}
}

// Draw the UI
// - Collected props
// - XVision bar
void GameManager::DrawUI()
{
	float w = GetRenderWidth();
	float h = GetRenderHeight();

	float fontSize = h * 0.02f;

	// UI rectangle, transparent
	Rectangle r = {
		w * 0.01f,
		h * 0.91f,
		w * 0.2f,
		h * 0.075f
	};
	float off = w * 0.0125f;
	Color back = { 0, 0, 0, 220 };
	DrawRectangle(r.x, r.y, r.width, r.height, back);

	// Collected props amount
	Vector2 p = { r.x + off, r.y + off * 0.5f };
	string s = "Collected   " + to_string(_collectedProps.size());
	DrawText(s.c_str(), p.x, p.y, fontSize, WHITE);
	
	// XVision text
	p = { r.x + off, r.y + off * 2 };
	DrawText("X Vision ", p.x, p.y, fontSize, WHITE);
	int m = MeasureText("X Vision ", fontSize);
	// Xvision bar: foreground rectangle inside of background rectangle
	Rectangle bar = { p.x + m + off, p.y, r.width * 0.5f, r.height * 0.2f };
	Color backBar = { 100, 100, 100, 200 };
	DrawRectangle(bar.x, bar.y, bar.width, bar.height, backBar);
	// Compute the horizontal size of the foreground rectangle
	float xPct = 1;
	if (_xVisionCooldownDeltaTime > 0) xPct = 1 - _xVisionCooldownDeltaTime / _xVisionCooldownTime;
	else if (_xVisionDeltaTime > 0) xPct = _xVisionDeltaTime / _xVisionTime;
	Color frontBar = { 250, 250, 250, 200 };
	int offP = 2;
	DrawRectangle(bar.x + offP, bar.y + offP, (bar.width - offP * 2) * xPct, bar.height - offP * 2, frontBar);
}

// Draw the exit door of the maze
void GameManager::DrawFinishDoor()
{
	float dir = (_direction - 0.5f) * 2.0f;
	float speed = 0.6f;
	// (For the lerp of the values)
	_topBottomValue += dir * speed * GetFrameTime();

	// Check if the last value has reached its target position to change the direction
	if (_direction == 1 && _topBottomValue > 1 || _direction == 0 && _topBottomValue < 0)
	{
		_direction = 1 - _direction;
		SetShaderValue(_finishDoorShader, _directionLoc, &_direction, SHADER_UNIFORM_FLOAT);
	}

	// Update the values
	_dividerTop = Lerp(-1.0f, 1.0f, _topBottomValue);
	_dividerBottom = Lerp(-0.0f, 2.0f, _topBottomValue);
	SetShaderValue(_finishDoorShader, _dividerTopLoc, &_dividerTop, SHADER_UNIFORM_FLOAT);
	SetShaderValue(_finishDoorShader, _dividerBottomLoc, &_dividerBottom, SHADER_UNIFORM_FLOAT);

	Vector3 f = _currentMap->GetFinishPosition();
	f.y -= _currentMap->GetScale() / 2.0f;
	DrawModel(_finishDoor, f, 1.0f, WHITE);
}


// Compute the transition in progress
void GameManager::ComputeTransition()
{
	_transitionPct += GetFrameTime() / _transitionTime;
	switch (_transition)
	{
		// Fade effect at the beginning of the maze
		case FadeOut:
			if (_transitionPct > 1)
			{
				// Transition completed
				_transitionPct = 0;
				_transition = NoEffect;
				PlayMusicStream(ResourcesManager::MazeTheme);
			}
			else
			{
				// Update fade effect
				float pixelSize = 100.0f * (1 - _transitionPct);
				SetShaderValue(_fadeShader, _pixelSizeLoc, &pixelSize, SHADER_UNIFORM_FLOAT);
				float whiteFactor = _transitionPct;
				SetShaderValue(_fadeShader, _whiteFactorLoc, &whiteFactor, SHADER_UNIFORM_FLOAT);
			}
			break;

		// Fade effect at the end of the maze
		case FadeIn:
			if (_transitionPct > 1)
			{
				// Transition completed
				if (_currentMapIdx == _cubicmaps.size() - 1)
				{
					// All mazes completed!
					StopMusicStream(ResourcesManager::MazeTheme);
					PlaySound(ResourcesManager::VictoryTheme);
					_state = Win;
				}
				else
				{
					// Still mazes remaining, show loading
					_transitionPct = 0;
					_transition = Loading;
					_transitionTime = 3;
				}
			}
			else
			{
				// Update fade effect
				float pixelSize = 100.0f * _transitionPct;
				SetShaderValue(_fadeShader, _pixelSizeLoc, &pixelSize, SHADER_UNIFORM_FLOAT);
				float whiteFactor = 1 - _transitionPct;
				SetShaderValue(_fadeShader, _whiteFactorLoc, &whiteFactor, SHADER_UNIFORM_FLOAT);
			}
			break;

		// Loading screen between mazes
		case Loading:
			if (_transitionPct > 1)
			{
				// Transition completed, so do a reset, next maze and fadeout effect
				delete _currentMap;
				_currentMap = nullptr;
				_currentMapIdx++;
				_currentMap = new MazeMap(_cubicmaps[_currentMapIdx], { 0.0f, 0.0f, 0.0f }, 1.0f);

				_player->Reset(_currentMap, _currentMap->GetStartPosition());
				Vector3 f = _currentMap->GetFinishPosition();
				float y = _currentMap->GetScale() / 2.0f;
				_finishDoorBody.min = _finishDoorBody.max = f;
				_finishDoorBody.min.y -= y;
				_finishDoorBody.min.y += y;
				//
				_transitionPct = 0;
				_transition = FadeOut;
				PlaySound(ResourcesManager::StartMazeSound);
				_transitionTime = 2;
			}
			break;
	}
}

GameManager::State GameManager::GetState()
{
	return _state;
}

void GameManager::CollectProp(Prop* prop)
{
	// Check this prop is not collected already
	if (find(_collectedProps.begin(), _collectedProps.end(), prop) == _collectedProps.end())
	{
		/*Vector3 pos = {0, _collectedProps.size() * 0.5f * -1.0f, 0};
		prop->SetParent(nullptr);
		prop->Translate(pos);
		//Vector3 v = prop->GetWorldPosition();
		std::cout << v.x << "," << v.y << "," << v.z << "\n";*/
		_collectedProps.push_back(prop);

		PlaySound(ResourcesManager::CollectItemSound);

		/*if (_maps[_currentMapIdx]->GetProps().empty())
		{
			_transitionPct = 0;
		}*/
	}
}

void GameManager::ActivateXVision(bool activate)
{
	ResourcesManager::Cubicmaps;

	// Still active or cooling down, so ignore
	if (_xVisionDeltaTime > 0 || _xVisionCooldownDeltaTime > 0) return;

	if (activate)
	{
		for (auto& p : _currentMap->GetProps())
			p->SetShader(_xVisionObjectShader);
		_xVisionCooldownDeltaTime = 0;
		_xVisionDeltaTime = _xVisionTime;
		PlayMusicStream(ResourcesManager::XVisionSound);
	}
	else
	{
		for (auto& p : _currentMap->GetProps())
			p->ResetShader();
		_xVisionDeltaTime = 0;
		_xVisionCooldownDeltaTime = _xVisionCooldownTime;
		StopMusicStream(ResourcesManager::XVisionSound);
	}
}

void GameManager::CheckFinishMaze(BoundingBox box)
{
	// if all props have been collected and the given box collides
	// with the exit door box, then finish the current maze
	if (_currentMap->GetProps().empty()
		&& CheckCollisionBoxes(box, _finishDoorBody))
	{
		for (auto& p : _collectedProps) delete p;
		_collectedProps.clear();

		// Disable XVision effect
		_xVisionDeltaTime = 0;
		_xVisionCooldownDeltaTime = _xVisionCooldownTime;
		
		_transitionPct = 0;
		_transition = FadeIn;
		_transitionTime = 2;
		StopMusicStream(ResourcesManager::MazeTheme);
		StopMusicStream(ResourcesManager::XVisionSound);
		PlaySound(ResourcesManager::FinishMazeSound);
	}
}

GameManager::~GameManager()
{
	StopMusicStream(ResourcesManager::MazeTheme);
	StopSound(ResourcesManager::FinishMazeSound);

	UnloadRenderTexture(_fullTexture);
	UnloadRenderTexture(_xTexture);

	for (auto& p : _collectedProps) delete p;
	_collectedProps.clear();

	delete _player;

	delete _currentMap;
}