#pragma once
#include "Actor.h"
#include "MazeMap.h"
#include <vector>

using namespace std;

class Player;

// Main class of the game
// Contains all the mazes, the player, the second camera
// and further information
class GameManager : public Actor
{
	public:
		enum Transition { NoEffect, FadeIn, FadeOut, Loading };
		enum State { None, Playing, Win, Loose };

	private:
		Transition _transition;
		State _state;
		//vector<MazeMap*> _maps;
		vector<Cubicmap> _cubicmaps;
		MazeMap* _currentMap;
		//
		Player* _player;
		int _currentMapIdx;
		//
		// Player's camera rendering
		Rectangle _fullViewport;
		Vector2 _fullViewPosition;
		RenderTexture _fullTexture;
		//
		// XVision's camera rendering
		Rectangle _xViewport;
		Vector2 _xViewPosition;
		Camera3D _xCamera;
		RenderTexture _xTexture;
		Vector3 _camPos;
		Vector3 _camTarget;
		//
		vector<Prop*> _collectedProps;
		//
		// Shader used for fade effect
		Shader _fadeShader;
		int _pixelSizeLoc;
		int _whiteFactorLoc;
		//
		// Data used for the exit door in mazes
		Model _finishDoor;
		BoundingBox _finishDoorBody;
		Shader _finishDoorShader;
		int _dividerTopLoc;
		int _dividerBottomLoc;
		int _directionLoc;
		float _dividerTop;
		float _dividerBottom;
		float _topBottomValue;
		float _direction;
		bool _showExitDoorCheat;
		//
		// Shaders used for XVision
		float _xVisionTime;
		float _xVisionDeltaTime;
		float _xVisionCooldownTime;
		float _xVisionCooldownDeltaTime;
		Shader _xVisionImageShader;
		Shader _xVisionObjectShader;
		bool _superXVisionCheat;
		//
		float _transitionTime;
		float _transitionPct;
		//
		void DrawUI();
		//
		void ComputeTransition();
		void DrawFinishDoor();

	public:
		GameManager();
		void Update() override;
		void Draw() override;
		State GetState();
		~GameManager() override;
		void CollectProp(Prop*);
		void ActivateXVision(bool);
		// Checks whether the given box collides with
		// the exit door
		void CheckFinishMaze(BoundingBox);
};
