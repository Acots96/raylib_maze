#pragma once
#include <raylib.h>
#include "raymath.h"
#include <vector>
#include <string>

using namespace std;

// Constants of Vector3 and Matrix
extern Vector3 const& zero3;
extern Vector3 const& one3;
extern Vector3 const& forward3;
extern Vector3 const& up3;
extern Vector3 const& right3;
extern Matrix const& identityM;

// Operations with Vector3 and Matrix overriden
Vector3 operator +(Vector3 v, Vector3 const& v2);
Vector3 operator -(Vector3 v, Vector3 const& v2);
Vector3 operator -(Vector3 v);
Vector3 operator *(Vector3 v, float const& f);
Vector3 operator /(Vector3 v, float const& f);
bool operator ==(Vector3 v, Vector3 const& v2);
bool operator !=(Vector3 v, Vector3 const& v2);
Vector3 operator *(Matrix const& m, Vector3 const& v);
Matrix operator *(Matrix const& m, float const& f);
Matrix operator *(Matrix const& m, Matrix const& m2);
Matrix Inverse(Matrix const& m);

// Struct containing the needed data for a cubicmap
// with objects and obstacles
using Cubicmap = struct Cubicmap
{
	using CubicmapObject = struct CubicmapObject
	{
		int type;
		Model model;
		Vector2 localPosition;
	};

	Vector2 size;
	Model model;
	vector<CubicmapObject> objects;
	vector<Vector2> obstacles;
};

// Given an image (maze), a sprite atlas size (max amount of textures
// that the map will have (squared number): 1, 4, 9, etc.) and a cube size,
// this method returns a Cubicmap object containing all the data 
// stored in the image as models and more, needed to draw a map
Cubicmap GenMeshCubicmapWithProps(Image cubicmap,
	int spriteAtlasSize, Vector3 cubeSize);
