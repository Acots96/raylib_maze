#include "OptionsScreen.h"
#include "Maze3D.h"

OptionsScreen::OptionsScreen(ScreensManager* manager, ScreenData* data) 
	: Screen(manager, data)
{
	_text = "Use A,D,W,S to move the character,\n"
		"Shift to run and the mouse to rotate the camera.\n\n"
		"Each maze can have a certain amount of objects,\n"
		"you need to collect them all to reveal the exit.\n"
		"You can press V to trigger Xvision to see\nwhere the objects are.\n\n\n"
		"Press 'O' to return to Title.";

	int textWidth = MeasureText(_text, _width * 0.02f);
	_textPosition = { (_width * 0.5f - textWidth * 0.67f), _height * 0.1f };
}

void OptionsScreen::Update()
{
	if (IsKeyReleased(KEY_O))
	{
		_manager->SwitchTo(Maze3D::Screens::TITLE);
	}
}

void OptionsScreen::Draw()
{
	BeginDrawing();

	ClearBackground(BLACK);
	DrawText(_text, _textPosition.x, _textPosition.y, _width / 45, WHITE);

	EndDrawing();
}
