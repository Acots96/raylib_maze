#include "RaylibUtils.h"

Vector3 const& zero3 = { 0.0f, 0.0f, 0.0f };
Vector3 const& one3 = { 1.0f, 1.0f, 1.0f };
Vector3 const& forward3 = { 0.0f, 0.0f, 1.0f };
Vector3 const& up3 = { 0.0f, 1.0f, 0.0f };
Vector3 const& right3 = { 1.0f, 0.0f, 0.0f };
Matrix const& identityM = {
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1,
};

Vector3 operator +(Vector3 v, Vector3 const& v2)
{
    return Vector3Add(v, v2);
}

Vector3 operator -(Vector3 v, Vector3 const& v2)
{
	return Vector3Subtract(v, v2);
}

Vector3 operator -(Vector3 v)
{
	return Vector3Negate(v);
}

Vector3 operator *(Vector3 v, float const& f)
{
	return Vector3Scale(v, f);
}

Vector3 operator /(Vector3 v, float const& f)
{
    return Vector3Divide(v, { f, f, f });
}

bool operator ==(Vector3 v, Vector3 const& v2)
{
	return Vector3Equals(v, v2);
}

bool operator !=(Vector3 v, Vector3 const& v2)
{
	return !Vector3Equals(v, v2);
}

Vector3 operator *(Matrix const& m, Vector3 const& v)
{
	return Vector3Transform(v, m);
}

Matrix operator *(Matrix const& m, float const& f)
{
    Matrix m2 = m;
    m2.m0 *= f; m2.m4 *= f; m2.m8 *= f; m2.m12 *= f;
    m2.m1 *= f; m2.m5 *= f; m2.m9 *= f; m2.m13 *= f;
    m2.m2 *= f; m2.m6 *= f; m2.m10 *= f; m2.m14 *= f;
    m2.m3 *= f; m2.m7 *= f; m2.m11 *= f; m2.m15 *= f;
    return m2;
}

Matrix operator *(Matrix const& m, Matrix const& m2)
{
    return MatrixMultiply(m, m2);
}

Matrix Inverse(Matrix const& m)
{
    return MatrixInvert(m);
}

//

Cubicmap GenMeshCubicmapWithProps(Image cubicmap, int spriteAtlasSize, Vector3 cubeSize)
{
    Mesh mesh = { 0 };

    Color* pixels = LoadImageColors(cubicmap);

    int mapWidth = cubicmap.width;
    int mapHeight = cubicmap.height;

    Cubicmap map{};
    map.size = { (float)mapWidth, (float)mapHeight };

    // NOTE: Max possible number of triangles numCubes*(12 triangles by cube)
    int maxTriangles = cubicmap.width * cubicmap.height * 12;

    int vCounter = 0;       // Used to count vertices
    int tcCounter = 0;      // Used to count texcoords
    int nCounter = 0;       // Used to count normals

    float w = cubeSize.x;
    float h = cubeSize.z;
    float h2 = cubeSize.y;

    Vector3* mapVertices = (Vector3*)RL_MALLOC(maxTriangles * 3 * sizeof(Vector3));
    Vector2* mapTexcoords = (Vector2*)RL_MALLOC(maxTriangles * 3 * sizeof(Vector2));
    Vector3* mapNormals = (Vector3*)RL_MALLOC(maxTriangles * 3 * sizeof(Vector3));

    // Define the 6 normals of the cube, we will combine them accordingly later...
    Vector3 n1 = { 1.0f, 0.0f, 0.0f };
    Vector3 n2 = { -1.0f, 0.0f, 0.0f };
    Vector3 n3 = { 0.0f, 1.0f, 0.0f };
    Vector3 n4 = { 0.0f, -1.0f, 0.0f };
    Vector3 n5 = { 0.0f, 0.0f, -1.0f };
    Vector3 n6 = { 0.0f, 0.0f, 1.0f };

    // NOTE: We use texture rectangles to define different textures for top-bottom-front-back-right-left (6)
    using RectangleF = struct RectangleF
    {
        float x;
        float y;
        float width;
        float height;

        // Used to reduce the size of the rectangle and change its position (to fit in sprite atlas)
        void divideAndOffset(float divider, Vector2 offset)
        {
            x = x / divider + offset.x;
            y = y / divider + offset.y;
            width = width / divider;
            height = height / divider;
        }
    };

    using Atlas = struct Atlas
    {
        RectangleF rightTexUV = { 0.0f, 0.0f, 0.5f, 0.5f };
        RectangleF leftTexUV = { 0.5f, 0.0f, 0.5f, 0.5f };
        RectangleF frontTexUV = { 0.0f, 0.0f, 0.5f, 0.5f };
        RectangleF backTexUV = { 0.5f, 0.0f, 0.5f, 0.5f };
        RectangleF topTexUV = { 0.0f, 0.5f, 0.5f, 0.5f };
        RectangleF bottomTexUV = { 0.5f, 0.5f, 0.5f, 0.5f };

        // Used to reduce the size of the rectangle and change its position (to fit in sprite atlas)
        void divideAndOffset(float divider, Vector2 offset)
        {
            rightTexUV.divideAndOffset(divider, offset);
            leftTexUV.divideAndOffset(divider, offset);
            frontTexUV.divideAndOffset(divider, offset);
            backTexUV.divideAndOffset(divider, offset);
            topTexUV.divideAndOffset(divider, offset);
            bottomTexUV.divideAndOffset(divider, offset);
        }
    };

    // Each texture in one atlas, stored as a vector to match the pixel blue value (0, 1, 2, 3)
    vector<Atlas> fullAtlas;
    fullAtlas.reserve(spriteAtlasSize);

    // Sprite atlas size squarerooted and maxed, for example: 4 sprites means that the sprite atlas
    // can have a size of 2x2, but 5 sprites means a sprite atlas of 3x3
    float divider = ceil(sqrt(spriteAtlasSize));

    // To match de sprite atlas, it needs to know how many textures will the atlas contain
    // and the position of each texture, so let's create a list of atlas (one per texture)
    // and tell them the corresponding rectangle by indicating its position (offset) and
    // its size (divider).
    float size01 = 1.0f / divider;
    for (int x = 0; x < divider; x++)
    {
        for (int y = 0; y < divider; y++)
        {
            auto atlas = Atlas();
            atlas.divideAndOffset(divider, { size01 * y, size01 * x });
            fullAtlas.push_back(atlas);
        }
    }

    for (int z = 0; z < mapHeight; ++z)
    {
        for (int x = 0; x < mapWidth; ++x)
        {
            // Define the 8 vertex of the cube, we will combine them accordingly later...
            Vector3 v1 = { w * (x - 0.5f), h2, h * (z - 0.5f) };
            Vector3 v2 = { w * (x - 0.5f), h2, h * (z + 0.5f) };
            Vector3 v3 = { w * (x + 0.5f), h2, h * (z + 0.5f) };
            Vector3 v4 = { w * (x + 0.5f), h2, h * (z - 0.5f) };
            Vector3 v5 = { w * (x + 0.5f), 0, h * (z - 0.5f) };
            Vector3 v6 = { w * (x - 0.5f), 0, h * (z - 0.5f) };
            Vector3 v7 = { w * (x - 0.5f), 0, h * (z + 0.5f) };
            Vector3 v8 = { w * (x + 0.5f), 0, h * (z + 0.5f) };

            Color pixel = pixels[z * cubicmap.width + x];

            // Use pixel's blue value to determine the texture needed to paint this map cube
            auto const& atlas = fullAtlas[pixel.b];

            // We check pixel color to be WHITE -> draw full cube
            if (pixel.r == 255)
            {
                // Define triangles and checking collateral cubes
                //------------------------------------------------

                // Define top triangles (2 tris, 6 vertex --> v1-v2-v3, v1-v3-v4)
                // WARNING: Not required for a WHITE cubes, created to allow seeing the map from outside
                mapVertices[vCounter] = v1;
                mapVertices[vCounter + 1] = v2;
                mapVertices[vCounter + 2] = v3;
                mapVertices[vCounter + 3] = v1;
                mapVertices[vCounter + 4] = v3;
                mapVertices[vCounter + 5] = v4;
                vCounter += 6;

                mapNormals[nCounter] = n3;
                mapNormals[nCounter + 1] = n3;
                mapNormals[nCounter + 2] = n3;
                mapNormals[nCounter + 3] = n3;
                mapNormals[nCounter + 4] = n3;
                mapNormals[nCounter + 5] = n3;
                nCounter += 6;

                mapTexcoords[tcCounter] = { atlas.topTexUV.x, atlas.topTexUV.y };
                mapTexcoords[tcCounter + 1] = { atlas.topTexUV.x, atlas.topTexUV.y + atlas.topTexUV.height };
                mapTexcoords[tcCounter + 2] = { atlas.topTexUV.x + atlas.topTexUV.width, atlas.topTexUV.y + atlas.topTexUV.height };
                mapTexcoords[tcCounter + 3] = { atlas.topTexUV.x, atlas.topTexUV.y };
                mapTexcoords[tcCounter + 4] = { atlas.topTexUV.x + atlas.topTexUV.width, atlas.topTexUV.y + atlas.topTexUV.height };
                mapTexcoords[tcCounter + 5] = { atlas.topTexUV.x + atlas.topTexUV.width, atlas.topTexUV.y };
                tcCounter += 6;

                // Define bottom triangles (2 tris, 6 vertex --> v6-v8-v7, v6-v5-v8)
                mapVertices[vCounter] = v6;
                mapVertices[vCounter + 1] = v8;
                mapVertices[vCounter + 2] = v7;
                mapVertices[vCounter + 3] = v6;
                mapVertices[vCounter + 4] = v5;
                mapVertices[vCounter + 5] = v8;
                vCounter += 6;

                mapNormals[nCounter] = n4;
                mapNormals[nCounter + 1] = n4;
                mapNormals[nCounter + 2] = n4;
                mapNormals[nCounter + 3] = n4;
                mapNormals[nCounter + 4] = n4;
                mapNormals[nCounter + 5] = n4;
                nCounter += 6;

                mapTexcoords[tcCounter] = { atlas.bottomTexUV.x + atlas.bottomTexUV.width, atlas.bottomTexUV.y };
                mapTexcoords[tcCounter + 1] = { atlas.bottomTexUV.x, atlas.bottomTexUV.y + atlas.bottomTexUV.height };
                mapTexcoords[tcCounter + 2] = { atlas.bottomTexUV.x + atlas.bottomTexUV.width, atlas.bottomTexUV.y + atlas.bottomTexUV.height };
                mapTexcoords[tcCounter + 3] = { atlas.bottomTexUV.x + atlas.bottomTexUV.width, atlas.bottomTexUV.y };
                mapTexcoords[tcCounter + 4] = { atlas.bottomTexUV.x, atlas.bottomTexUV.y };
                mapTexcoords[tcCounter + 5] = { atlas.bottomTexUV.x, atlas.bottomTexUV.y + atlas.bottomTexUV.height };
                tcCounter += 6;

                // Checking cube on bottom of current cube
                int c = pixels[(z + 1) * cubicmap.width + x].r;
                if (((z < cubicmap.height - 1) && (c == 0 || c == 128)) || (z == cubicmap.height - 1))
                {
                    // Define front triangles (2 tris, 6 vertex) --> v2 v7 v3, v3 v7 v8
                    // NOTE: Collateral occluded faces are not generated
                    mapVertices[vCounter] = v2;
                    mapVertices[vCounter + 1] = v7;
                    mapVertices[vCounter + 2] = v3;
                    mapVertices[vCounter + 3] = v3;
                    mapVertices[vCounter + 4] = v7;
                    mapVertices[vCounter + 5] = v8;
                    vCounter += 6;

                    mapNormals[nCounter] = n6;
                    mapNormals[nCounter + 1] = n6;
                    mapNormals[nCounter + 2] = n6;
                    mapNormals[nCounter + 3] = n6;
                    mapNormals[nCounter + 4] = n6;
                    mapNormals[nCounter + 5] = n6;
                    nCounter += 6;

                    mapTexcoords[tcCounter] = { atlas.frontTexUV.x, atlas.frontTexUV.y };
                    mapTexcoords[tcCounter + 1] = { atlas.frontTexUV.x, atlas.frontTexUV.y + atlas.frontTexUV.height };
                    mapTexcoords[tcCounter + 2] = { atlas.frontTexUV.x + atlas.frontTexUV.width, atlas.frontTexUV.y };
                    mapTexcoords[tcCounter + 3] = { atlas.frontTexUV.x + atlas.frontTexUV.width, atlas.frontTexUV.y };
                    mapTexcoords[tcCounter + 4] = { atlas.frontTexUV.x, atlas.frontTexUV.y + atlas.frontTexUV.height };
                    mapTexcoords[tcCounter + 5] = { atlas.frontTexUV.x + atlas.frontTexUV.width, atlas.frontTexUV.y + atlas.frontTexUV.height };
                    tcCounter += 6;
                }

                // Checking cube on top of current cube
                c = pixels[(z - 1) * cubicmap.width + x].r;
                if (((z > 0) && (c == 0 || c == 128)) || (z == 0))
                {
                    // Define back triangles (2 tris, 6 vertex) --> v1 v5 v6, v1 v4 v5
                    // NOTE: Collateral occluded faces are not generated
                    mapVertices[vCounter] = v1;
                    mapVertices[vCounter + 1] = v5;
                    mapVertices[vCounter + 2] = v6;
                    mapVertices[vCounter + 3] = v1;
                    mapVertices[vCounter + 4] = v4;
                    mapVertices[vCounter + 5] = v5;
                    vCounter += 6;

                    mapNormals[nCounter] = n5;
                    mapNormals[nCounter + 1] = n5;
                    mapNormals[nCounter + 2] = n5;
                    mapNormals[nCounter + 3] = n5;
                    mapNormals[nCounter + 4] = n5;
                    mapNormals[nCounter + 5] = n5;
                    nCounter += 6;

                    mapTexcoords[tcCounter] = { atlas.backTexUV.x + atlas.backTexUV.width, atlas.backTexUV.y };
                    mapTexcoords[tcCounter + 1] = { atlas.backTexUV.x, atlas.backTexUV.y + atlas.backTexUV.height };
                    mapTexcoords[tcCounter + 2] = { atlas.backTexUV.x + atlas.backTexUV.width, atlas.backTexUV.y + atlas.backTexUV.height };
                    mapTexcoords[tcCounter + 3] = { atlas.backTexUV.x + atlas.backTexUV.width, atlas.backTexUV.y };
                    mapTexcoords[tcCounter + 4] = { atlas.backTexUV.x, atlas.backTexUV.y };
                    mapTexcoords[tcCounter + 5] = { atlas.backTexUV.x, atlas.backTexUV.y + atlas.backTexUV.height };
                    tcCounter += 6;
                }

                // Checking cube on right of current cube
                c = pixels[z * cubicmap.width + (x + 1)].r;
                if (((x < cubicmap.width - 1) && (c == 0 || c == 128)) || (x == cubicmap.width - 1))
                {
                    // Define right triangles (2 tris, 6 vertex) --> v3 v8 v4, v4 v8 v5
                    // NOTE: Collateral occluded faces are not generated
                    mapVertices[vCounter] = v3;
                    mapVertices[vCounter + 1] = v8;
                    mapVertices[vCounter + 2] = v4;
                    mapVertices[vCounter + 3] = v4;
                    mapVertices[vCounter + 4] = v8;
                    mapVertices[vCounter + 5] = v5;
                    vCounter += 6;

                    mapNormals[nCounter] = n1;
                    mapNormals[nCounter + 1] = n1;
                    mapNormals[nCounter + 2] = n1;
                    mapNormals[nCounter + 3] = n1;
                    mapNormals[nCounter + 4] = n1;
                    mapNormals[nCounter + 5] = n1;
                    nCounter += 6;

                    mapTexcoords[tcCounter] = { atlas.rightTexUV.x, atlas.rightTexUV.y };
                    mapTexcoords[tcCounter + 1] = { atlas.rightTexUV.x, atlas.rightTexUV.y + atlas.rightTexUV.height };
                    mapTexcoords[tcCounter + 2] = { atlas.rightTexUV.x + atlas.rightTexUV.width, atlas.rightTexUV.y };
                    mapTexcoords[tcCounter + 3] = { atlas.rightTexUV.x + atlas.rightTexUV.width, atlas.rightTexUV.y };
                    mapTexcoords[tcCounter + 4] = { atlas.rightTexUV.x, atlas.rightTexUV.y + atlas.rightTexUV.height };
                    mapTexcoords[tcCounter + 5] = { atlas.rightTexUV.x + atlas.rightTexUV.width, atlas.rightTexUV.y + atlas.rightTexUV.height };
                    tcCounter += 6;
                }

                // Checking cube on left of current cube
                c = pixels[z * cubicmap.width + (x - 1)].r;
                if (((x > 0) && (c == 0 || c == 128)) || (x == 0))
                {
                    // Define left triangles (2 tris, 6 vertex) --> v1 v7 v2, v1 v6 v7
                    // NOTE: Collateral occluded faces are not generated
                    mapVertices[vCounter] = v1;
                    mapVertices[vCounter + 1] = v7;
                    mapVertices[vCounter + 2] = v2;
                    mapVertices[vCounter + 3] = v1;
                    mapVertices[vCounter + 4] = v6;
                    mapVertices[vCounter + 5] = v7;
                    vCounter += 6;

                    mapNormals[nCounter] = n2;
                    mapNormals[nCounter + 1] = n2;
                    mapNormals[nCounter + 2] = n2;
                    mapNormals[nCounter + 3] = n2;
                    mapNormals[nCounter + 4] = n2;
                    mapNormals[nCounter + 5] = n2;
                    nCounter += 6;

                    mapTexcoords[tcCounter] = { atlas.leftTexUV.x, atlas.leftTexUV.y };
                    mapTexcoords[tcCounter + 1] = { atlas.leftTexUV.x + atlas.leftTexUV.width, atlas.leftTexUV.y + atlas.leftTexUV.height };
                    mapTexcoords[tcCounter + 2] = { atlas.leftTexUV.x + atlas.leftTexUV.width, atlas.leftTexUV.y };
                    mapTexcoords[tcCounter + 3] = { atlas.leftTexUV.x, atlas.leftTexUV.y };
                    mapTexcoords[tcCounter + 4] = { atlas.leftTexUV.x, atlas.leftTexUV.y + atlas.leftTexUV.height };
                    mapTexcoords[tcCounter + 5] = { atlas.leftTexUV.x + atlas.leftTexUV.width, atlas.leftTexUV.y + atlas.leftTexUV.height };
                    tcCounter += 6;
                }

                // It is an objstacle, so it must be added to the obstacles list
                map.obstacles.push_back({ (float)x, (float)z });
            }
            // We check pixel color to be BLACK, we will only draw floor and roof
            // (128 for objects in map that are not obstacles)
            else if (pixel.r == 0 || pixel.r == 128)
            {
                // Define top triangles (2 tris, 6 vertex --> v1-v2-v3, v1-v3-v4)
                mapVertices[vCounter] = v1;
                mapVertices[vCounter + 1] = v3;
                mapVertices[vCounter + 2] = v2;
                mapVertices[vCounter + 3] = v1;
                mapVertices[vCounter + 4] = v4;
                mapVertices[vCounter + 5] = v3;
                vCounter += 6;

                mapNormals[nCounter] = n4;
                mapNormals[nCounter + 1] = n4;
                mapNormals[nCounter + 2] = n4;
                mapNormals[nCounter + 3] = n4;
                mapNormals[nCounter + 4] = n4;
                mapNormals[nCounter + 5] = n4;
                nCounter += 6;

                mapTexcoords[tcCounter] = { atlas.topTexUV.x, atlas.topTexUV.y };
                mapTexcoords[tcCounter + 1] = { atlas.topTexUV.x + atlas.topTexUV.width, atlas.topTexUV.y + atlas.topTexUV.height };
                mapTexcoords[tcCounter + 2] = { atlas.topTexUV.x, atlas.topTexUV.y + atlas.topTexUV.height };
                mapTexcoords[tcCounter + 3] = { atlas.topTexUV.x, atlas.topTexUV.y };
                mapTexcoords[tcCounter + 4] = { atlas.topTexUV.x + atlas.topTexUV.width, atlas.topTexUV.y };
                mapTexcoords[tcCounter + 5] = { atlas.topTexUV.x + atlas.topTexUV.width, atlas.topTexUV.y + atlas.topTexUV.height };
                tcCounter += 6;

                // Define bottom triangles (2 tris, 6 vertex --> v6-v8-v7, v6-v5-v8)
                mapVertices[vCounter] = v6;
                mapVertices[vCounter + 1] = v7;
                mapVertices[vCounter + 2] = v8;
                mapVertices[vCounter + 3] = v6;
                mapVertices[vCounter + 4] = v8;
                mapVertices[vCounter + 5] = v5;
                vCounter += 6;

                mapNormals[nCounter] = n3;
                mapNormals[nCounter + 1] = n3;
                mapNormals[nCounter + 2] = n3;
                mapNormals[nCounter + 3] = n3;
                mapNormals[nCounter + 4] = n3;
                mapNormals[nCounter + 5] = n3;
                nCounter += 6;

                mapTexcoords[tcCounter] = { atlas.bottomTexUV.x + atlas.bottomTexUV.width, atlas.bottomTexUV.y };
                mapTexcoords[tcCounter + 1] = { atlas.bottomTexUV.x + atlas.bottomTexUV.width, atlas.bottomTexUV.y + atlas.bottomTexUV.height };
                mapTexcoords[tcCounter + 2] = { atlas.bottomTexUV.x, atlas.bottomTexUV.y + atlas.bottomTexUV.height };
                mapTexcoords[tcCounter + 3] = { atlas.bottomTexUV.x + atlas.bottomTexUV.width, atlas.bottomTexUV.y };
                mapTexcoords[tcCounter + 4] = { atlas.bottomTexUV.x, atlas.bottomTexUV.y + atlas.bottomTexUV.height };
                mapTexcoords[tcCounter + 5] = { atlas.bottomTexUV.x, atlas.bottomTexUV.y };
                tcCounter += 6;

                // Object
                if (pixel.r == 128)
                {
                    auto p = Cubicmap::CubicmapObject();
                    p.type = pixel.g;
                    p.localPosition = { (float)x, (float)z };
                    map.objects.push_back(p);
                }
            }
        }
    }

    // Move data from mapVertices temp arays to vertices float array
    mesh.vertexCount = vCounter;
    mesh.triangleCount = vCounter / 3;

    mesh.vertices = (float*)RL_MALLOC(mesh.vertexCount * 3 * sizeof(float));
    mesh.normals = (float*)RL_MALLOC(mesh.vertexCount * 3 * sizeof(float));
    mesh.texcoords = (float*)RL_MALLOC(mesh.vertexCount * 2 * sizeof(float));
    mesh.colors = NULL;

    int fCounter = 0;

    // Move vertices data
    for (int i = 0; i < vCounter; i++)
    {
        mesh.vertices[fCounter] = mapVertices[i].x;
        mesh.vertices[fCounter + 1] = mapVertices[i].y;
        mesh.vertices[fCounter + 2] = mapVertices[i].z;
        fCounter += 3;
    }

    fCounter = 0;

    // Move normals data
    for (int i = 0; i < nCounter; i++)
    {
        mesh.normals[fCounter] = mapNormals[i].x;
        mesh.normals[fCounter + 1] = mapNormals[i].y;
        mesh.normals[fCounter + 2] = mapNormals[i].z;
        fCounter += 3;
    }

    fCounter = 0;

    // Move texcoords data
    for (int i = 0; i < tcCounter; i++)
    {
        mesh.texcoords[fCounter] = mapTexcoords[i].x;
        mesh.texcoords[fCounter + 1] = mapTexcoords[i].y;
        fCounter += 2;
    }

    RL_FREE(mapVertices);
    RL_FREE(mapNormals);
    RL_FREE(mapTexcoords);

    UnloadImageColors(pixels);   // Unload pixels color data

    // Upload vertex data to GPU (static mesh)
    UploadMesh(&mesh, false);

    map.model = LoadModelFromMesh(mesh);

    return map;
}
