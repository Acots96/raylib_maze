#pragma once
#include <raylib.h>
#include "ScreensManager.h"
#include <memory>
#include <map>

using namespace std;

// Main class of the game.
// Holds an instance of the active screen and allows
// to change to another screen.
class Maze3D : public ScreensManager
{
	public:
		enum Screens
		{
			TITLE = 0,
			OPTIONS = 1,
			GAMEPLAY = 2,
			ENDING = 3
		};

	public:
		Maze3D();
		void SwitchTo(int screen, ScreenData* data = nullptr) override;
		void Update();
		~Maze3D();
};
