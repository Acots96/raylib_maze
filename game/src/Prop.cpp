#include "Prop.h"
#include <iostream>

void Prop::Update()
{
	// Slow rotation effect (Y axis)
	_axisYAngle = _axisYAngle + (_rotateSpeed * GetFrameTime());
}

void Prop::Draw()
{
	DrawModelEx(_model, GetWorldPosition(), up3, _axisYAngle, { _scale, _scale, _scale }, WHITE);
}

BoundingBox Prop::GetBody()
{
	return _body;
}

void Prop::SetScale(float scale)
{
	_scale = scale;
}

void Prop::SetModel(const Model& model)
{
	_model = model;
	_defaultShader = _model.materials[0].shader;
}

void Prop::SetShader(Shader shader)
{
	_model.materials[0].shader = shader;
}
void Prop::ResetShader()
{
	_model.materials[0].shader = _defaultShader;
}

void Prop::ComputeBody()
{
	ComputeBody(_scale);
}
void Prop::ComputeBody(float scale)
{
	auto body = BoundingBox();
	Vector3 p = GetWorldPosition();
	body.min = p - one3 * scale;
	body.max = p + one3 * scale;
	ComputeBody(body);
}
void Prop::ComputeBody(const BoundingBox& body)
{
	_body = body;
}

Prop::~Prop()
{

}