#include "ResourcesManager.h"
#include <iostream>
#include <string>
#include <fstream>
#include <filesystem>

// Textures

Texture2D ResourcesManager::TitleImage;
Texture2D ResourcesManager::AtlasImage;

void ResourcesManager::LoadTextures()
{
	TitleImage = LoadTexture("resources/title.png");
	AtlasImage = LoadTexture("resources/cubemap_atlas_full.png");
}

void ResourcesManager::UnloadTextures()
{
	UnloadTexture(TitleImage);
	UnloadTexture(AtlasImage);
}

// Models

vector<Model> ResourcesManager::Props;
vector<Cubicmap> ResourcesManager::Cubicmaps;

void ResourcesManager::LoadModels()
{
	// 254 is the max number of props
	Props.assign(254, Model());

	vector<Model> others {};

	// A 3D object file must be named 'ABCD_XYZ.obj' and its diffuse texture 'ABCD_diffuse_XYZ.png'
	// where, 'ABCD' is the name and 'XYZ' is the number related to the maze image.
	// 3D objects without number ('ABCD.obj' and 'ABCD_diffuse.png') will be assigned a random number

	for (auto& p: filesystem::recursive_directory_iterator("resources/models/obj/"))
	{
		if (string ext = p.path().extension().string(); ext != ".obj") continue;

		try
		{
			// Get the obj file with its diffuse texture, both numbered as expected
			string fileName = p.path().string();
			fileName = fileName.substr(0, fileName.find_last_of("."));
			string i = fileName.substr(fileName.find_last_of("_") + 1, fileName.size() - 1);
			int idx = stoi(i);

			string textureUri = p.path().filename().string();
			textureUri = textureUri.append("_diffuse_" + i + ".png");

			Model m = LoadModel(p.path().string().c_str());
			m.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = LoadTexture(textureUri.c_str());
			Props[idx] = m;
		}
		catch (exception e)
		{
			// Failing means that obj file or texture file is not numbered,
			// so try now to get the files without number and failing now means
			// there is no corresponding texture file for the obj file
			string textureUri = p.path().string();
			textureUri = textureUri.substr(0, textureUri.find_last_of("."));
			textureUri = textureUri.append("_diffuse.png");

			try
			{
				std::cout << "====================== " << p.path().string().c_str() << "\n";
				Model m = LoadModel(p.path().string().c_str());
				m.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = LoadTexture(textureUri.c_str());
				others.push_back(m);
			}
			catch (exception e)
			{
				std::cerr << "A 3D object file must be named 'ABCD_XYZ.obj' and its diffuse texture 'ABCD_diffuse_XYZ.png', where 'ABCD' is the name and 'XYZ' is the number related to the maze image. 3D objects without number ('ABCD.obj' and 'ABCD_diffuse.png') will be assigned a random number.\n";
			}
		}
	}

	// Assign the objects without number (if any) to fill the blank spaces in the props list
	if (!others.empty())
	{
		for (int i = 1; i < Props.size(); i++)
		{
			if (auto& p = Props[i]; p.meshCount == 0)
			{
				Model m = others[others.size() - 1];
				others.pop_back();
				p = LoadModelFromMesh(m.meshes[0]);
				p.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = m.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture;
				Props[i] = p;
			}
			if (others.empty()) break;
		}
	}
}

void ResourcesManager::UnloadModels()
{
	for (auto const& model : Props)
		UnloadModel(model);
	Props.clear();

	for (auto& map : Cubicmaps)
	{
		UnloadModel(map.model);
		for (auto& prop : map.objects)
			UnloadModel(prop.model);
		map.objects.clear();
	}
	Cubicmaps.clear();
}

// Maps

void ResourcesManager::LoadMaps()
{
	for (auto& p : filesystem::recursive_directory_iterator("resources/maps/"))
	{
		if (p.path().extension().string() != ".png") continue;
		string name = p.path().string();

		try
		{
			Image mapImg = LoadImage(name.c_str());

			Cubicmap map = GenMeshCubicmapWithProps(mapImg, 4, { 1.0f, 1.0f, 1.0f });
			map.model.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = AtlasImage;

			for (auto& prop : map.objects)
			{
				if (prop.type >= Props.size())
				{
					std::cout << "Cannot found obejct type " << prop.type << "\n";
					continue;
				}
				prop.model = Props[prop.type];
			}

			Cubicmaps.push_back(map);
			//ExportMesh(map.model.meshes[0], "resources/maze1mesh.obj");
		}
		catch (exception e)
		{
			std::cerr << "Something went wrong when loading '" << name << "'\n";
		}
	}
}

// Audio

Sound ResourcesManager::IntroTheme;
Music ResourcesManager::MazeTheme;
Sound ResourcesManager::StartMazeSound;
Sound ResourcesManager::FinishMazeSound;
Sound ResourcesManager::StepSound;
Sound ResourcesManager::CollectItemSound;
Music ResourcesManager::XVisionSound;
Sound ResourcesManager::VictoryTheme;

void ResourcesManager::LoadSounds()
{
	IntroTheme = LoadSound("resources/audio/Intro.wav");
	MazeTheme = LoadMusicStream("resources/audio/MazeTheme.mp3");
	MazeTheme.looping = true;
	SetMusicVolume(MazeTheme, 0.4f);
	StartMazeSound = LoadSound("resources/audio/StartMaze.wav");
	FinishMazeSound = LoadSound("resources/audio/StartMaze.wav");
	StepSound = LoadSound("resources/audio/Step.ogg");
	CollectItemSound = LoadSound("resources/audio/CollectItem.wav");
	XVisionSound = LoadMusicStream("resources/audio/XVisionLoop.wav");
	XVisionSound.looping = true;
	VictoryTheme = LoadSound("resources/audio/Victory.wav");
}

void ResourcesManager::UnloadSounds()
{
	UnloadSound(IntroTheme);
	UnloadMusicStream(MazeTheme);
	UnloadSound(StartMazeSound);
	UnloadSound(FinishMazeSound);
	UnloadSound(StepSound);
	UnloadSound(CollectItemSound);
	UnloadMusicStream(XVisionSound);
	UnloadSound(VictoryTheme);
}

//

Shader ResourcesManager::FadeShader;
Shader ResourcesManager::FinishDoorShader;
Shader ResourcesManager::XVisionImageShader;
Shader ResourcesManager::XVisionObjectShader;

void ResourcesManager::LoadShaders()
{
	FadeShader = LoadShader(nullptr, "resources/shaders/pixelizer.fs");
	FinishDoorShader = LoadShader(nullptr, "resources/shaders/color_mix.fs");
	XVisionImageShader = LoadShader(nullptr, "resources/shaders/grayscale.fs");
	XVisionObjectShader = LoadShader(nullptr, "resources/shaders/redscale.fs");
}

void ResourcesManager::UnloadShaders()
{
	UnloadShader(FadeShader);
	UnloadShader(FinishDoorShader);
	UnloadShader(XVisionImageShader);
	UnloadShader(XVisionObjectShader);
}

//

void ResourcesManager::LoadResources()
{
	LoadTextures();
	LoadModels();
	LoadMaps();
	LoadSounds();
	LoadShaders();
}

void ResourcesManager::UnloadResources()
{
	UnloadTextures();
	UnloadModels();
	UnloadSounds();
	UnloadShaders();
}