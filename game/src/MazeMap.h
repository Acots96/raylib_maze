#pragma once
#include "Actor.h"
#include <raylib.h>
#include <vector>
#include <tuple>
#include <string>
#include "RaylibUtils.h"
#include "Prop.h"

using namespace std;

// Represents a maze map
class MazeMap : public Actor
{
	private:
		// Maze
		Model _model;
		list<Prop*> _props;
		// Designed to have the size of the whole maze,
		// but only the obstacle cells (walls) are set to true
		vector<tuple<bool, BoundingBox>> _obstacles;
		float _scale;
		//
		Vector3 _startPos;
		Vector3 _finishPos;

	public:
		explicit MazeMap() = default;
		explicit MazeMap(const Cubicmap& mapData,
			Vector3 position, float scale);
		void Update() override;
		void Draw() override;
		float GetScale();
		Vector3 GetStartPosition();
		Vector3 GetFinishPosition();
		// Checks if the given box collides with any obstacle.
		// If true, fills the given RayCollision
		bool IsColliding(BoundingBox, RayCollision&);
		// Checks if the given box is colliding with any prop.
		// If true, removes it from the maze and returns it to the caller
		Prop* IsTouchingProp(BoundingBox box);
		list<Prop*> GetProps();
		~MazeMap() override;
};
