#include "EndingScreen.h"
#include "ResourcesManager.h"
#include "Maze3D.h"
#include "EndingScreenData.h"

EndingScreen::EndingScreen(ScreensManager* manager, ScreenData* data)
	: Screen(manager, data)
{
	bool win = false;
	if (data != nullptr)
	{
		win = ((EndingScreenData*)data)->playerWin;
	}

	if (win)
	{
		PlaySound(ResourcesManager::VictoryTheme);
		_text = "You finished all the mazes! :D"
			"\n\nPress Enter for Title\n"
			"Press 'O' for Options";
	}
	/*else
	{
		//PlaySound(ResourcesManager::GameOverTheme);
		_text = "You loose... :("
			"\n\nPress Enter for Title\n"
			"Press 'O' for Options";
	}*/

	int textWidth = MeasureText(_text, _width / 45);
	_textPosition = { (float)(_width / 2 - textWidth / 2), _height * 0.2f };
}

void EndingScreen::Update()
{
	if (IsKeyReleased(KEY_O))
	{
		_manager->SwitchTo(Maze3D::Screens::OPTIONS);
	}
	else if (IsKeyReleased(KEY_ENTER))
	{
		_manager->SwitchTo(Maze3D::Screens::TITLE);
	}
}

void EndingScreen::Draw()
{
	BeginDrawing();

	ClearBackground(BLACK);
	DrawText(_text, _textPosition.x, _textPosition.y, _width / 45, WHITE);

	EndDrawing();
}

EndingScreen::~EndingScreen()
{
	StopSound(ResourcesManager::VictoryTheme);
	//StopSound(ResourcesManager::GameOverTheme);
}
