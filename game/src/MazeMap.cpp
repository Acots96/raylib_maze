#include "MazeMap.h"
#include <iostream>

float ComputeScale(BoundingBox box, Vector3 maxSize, float defaultScale = 1.0f);

MazeMap::MazeMap(const Cubicmap& mapData, Vector3 position, float scale) : Actor()
{
	_model = mapData.model;
	Translate(position);
	_scale = scale;

	// Fills the whole vector with empty tuples and then uses the position
	// of the obstacles stored in the Cubicmap to calculate the index
	auto t = tuple<bool, BoundingBox>(false, BoundingBox());
	_obstacles.assign(mapData.size.x * mapData.size.y, t);
	for (auto& obj : mapData.obstacles)
	{
		auto b = BoundingBox();
		Vector3 p = { obj.x, scale / 2, obj.y };
		Vector3 off = one3 / 2 * scale;
		b.min = TransformLocalToWorld(p - off);
		b.max = TransformLocalToWorld(p + off);
		// Calculate the index and set this cell to true
		_obstacles[obj.y * mapData.size.x + obj.x] = tuple<bool, BoundingBox>(true, b);
	}

	// Each object stored in the Cubicmap is loaded as a model and stored in
	// props list, except for the cases 254 and 255 which are considered the
	// start and finish positions (respectively) of the maze
	_props = {};
	int start = -1, finish = -1;
	for (auto& obj: mapData.objects)
	{
		auto v = obj.localPosition;
		switch (obj.type)
		{
			case 254:
				_startPos = TransformLocalToWorld({ v.x, 0, v.y });
				_startPos.y = position.y + _scale / 2.0f;
				start = v.y * mapData.size.x + v.y;
				break;

			case 255:
				_finishPos = TransformLocalToWorld({ v.x, 0, v.y });
				_finishPos.y = position.y + _scale / 2.0f;
				finish = v.y * mapData.size.x + v.y;
				break;

			default:
				if (obj.model.meshCount == 0)
					break;
				auto p = new Prop();
				p->SetModel(obj.model);
				// Models are not intended to have their real size because they need
				// to fit in maze cells, so the new scale of the model is calculated
				// based on the maze cell scale.
				float scale = ComputeScale(GetModelBoundingBox(obj.model), { 0.5f, 0.5f, 0.5f }, _scale);
				//p->SetScale(_scale * 0.02f);
				p->SetScale(scale);
				p->SetParent(this);
				p->Translate({ v.x, 0, v.y });
				p->ComputeBody(0.25f);
				_props.push_back(p);
				break;
		}
	}

	// If start or finish are not defined in Cubicmap, they'll be randomly assigned
	if (start == -1 || finish == -1)
	{
		vector<bool> map;
		map.assign(mapData.size.x * mapData.size.y, true);
		for (auto o : mapData.obstacles)
			map[o.y * mapData.size.x + o.x] = false;
		for (auto o : mapData.objects)
			map[o.localPosition.y * mapData.size.x + o.localPosition.x] = false;

		// Get a random int to use it as start index and move "forward" through
		// the maze until find an empty position to mark as the start. If the end
		// of the maze is reached, then go back to the beginning and move forward
		// until the previous position used at the beginning

		int startIdx = ((float)rand() / (float)RAND_MAX) * 255;
		int maxTries = map.size() - 1;
		if (start == -1)
		{
			do
			{
				// Is it free space?
				if (map[startIdx])
				{
					// position = row * rowSize + pInRow
					// pInRow = position - row * rowSize
					// row = (position - pInRow) / rowSize
					float col = startIdx % (int)mapData.size.x;
					float row = (startIdx - col) / (int)mapData.size.x;
					_startPos = TransformLocalToWorld({ col , 0, row });
					_startPos.y = position.y + _scale / 2.0f;
					start = startIdx;
				}
				else
				{
					startIdx++;
					if (startIdx == map.size()) startIdx = 0;
					maxTries--;
				}
			} while (maxTries >= 0 && start == -1);

			if (!start)
				throw exception("Entrance should be determined as an object with number g=254,"
					" or at least the maze should have enough free space to randomly assign it.");
		}
		else startIdx = start;

		// Same for the finish

		int finishIdx = ((float)rand() / (float)RAND_MAX) * 255;
		maxTries = map.size() - 1;
		if (finish == -1)
		{
			do
			{
				// Is it free space AND is not the start position?
				if (map[finishIdx] && finishIdx != startIdx)
				{
					float col = finishIdx % (int)mapData.size.x;
					float row = (finishIdx - col) / (int)mapData.size.x;
					_finishPos = TransformLocalToWorld({ col , 0, row });
					_finishPos.y = position.y + _scale / 2.0f;
					finish = finishIdx;
				}
				else
				{
					finishIdx++;
					if (finishIdx == map.size()) finishIdx = 0;
					maxTries--;
				}
			} while (maxTries >= 0 && finish == -1);

			if (!finish)
				throw exception("Exit should be determined as an object with number g=255,"
					" or at least the maze should have enough free space to randomly assign it.");
		}
	}
}

// Given a box determining the current size of an object, a max size
// and a default scale, this method calculates a new scale for the object
// if it is too big, decreasing the given size evenly
float ComputeScale(BoundingBox box, Vector3 maxSize, float defaultScale)
{
	Vector3 size = { box.max.x - box.min.x, box.max.y - box.min.y, box.max.z - box.min.z };

	if (size.x > maxSize.x)
	{
		// X size too big? calculate new default scale so size.x <= mnaxSize.x
		defaultScale *= maxSize.x / size.x;
		size = size * defaultScale;
	}

	if (size.y > maxSize.y)
	{
		// Y size too big? calculate new default scale so size.y <= mnaxSize.y
		defaultScale *= maxSize.y / size.y;
		size = size * defaultScale;
	}

	if (size.z > maxSize.z)
	{
		// Z size too big? calculate new default scale so size.z <= mnaxSize.z
		defaultScale *= maxSize.z / size.z;
		size = size * defaultScale;
	}

	return defaultScale;
}

void MazeMap::Update()
{
	for (auto prop : _props)
	{
		prop->Update();
	}
}

void MazeMap::Draw()
{
	DrawModel(_model, GetWorldPosition(), _scale, WHITE);
	for (auto prop : _props)
	{
		prop->Draw();
	}
}

float MazeMap::GetScale()
{
	return _scale;
}
Vector3 MazeMap::GetStartPosition()
{
	return _startPos;
}
Vector3 MazeMap::GetFinishPosition()
{
	return _finishPos;
}

bool MazeMap::IsColliding(BoundingBox box, RayCollision& collision)
{
	Vector3 p0 = (box.max + box.min) * 0.5f;
	for (const auto& [isObstacle, b] : _obstacles)
	{
		if (!isObstacle) continue;
		if (CheckCollisionBoxes(box, b))
		{
			Vector3 p1 = (b.max + b.min) * 0.5f;
			Ray r = { p0, Vector3Normalize(p1 - p0) };
			collision = GetRayCollisionBox(r, b);
			return true;
		}
	}
	return false;
}

Prop* MazeMap::IsTouchingProp(BoundingBox box)
{
	for (auto p : _props)
	{
		if (CheckCollisionBoxes(box, p->GetBody()))
		{
			p->ResetShader();
			_props.remove(p);
			return p;
		}
	}
	return nullptr;
}

list<Prop*> MazeMap::GetProps()
{
	return _props;
}

MazeMap::~MazeMap()
{
	//UnloadModel(_model);

	for (auto& p : _props) delete p;
	_props.clear();
	_obstacles.clear();
}