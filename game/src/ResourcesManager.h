#pragma once
#include <raylib.h>
#include <vector>
#include <string>
#include "RaylibUtils.h"

using namespace std;

// Class holding all the resources, including Load/Unload methods.
class ResourcesManager
{
	private:
		static void LoadMaps();
		static void LoadTextures();
		static void UnloadTextures();
		static void LoadModels();
		static void UnloadModels();
		static void LoadSounds();
		static void UnloadSounds();
		static void LoadShaders();
		static void UnloadShaders();

	public:
		// Textures
		static Texture2D TitleImage;
		static Texture2D AtlasImage;
		// Models
		static vector<Model> Props;
		// Maps
		static vector<Cubicmap> Cubicmaps;
		// Audio
		static Sound IntroTheme;
		static Music MazeTheme;
		static Sound StartMazeSound;
		static Sound FinishMazeSound;
		static Sound StepSound;
		static Sound CollectItemSound;
		static Music XVisionSound;
		static Sound VictoryTheme;
		// Shader
		static Shader FadeShader;
		static Shader FinishDoorShader;
		static Shader XVisionImageShader;
		static Shader XVisionObjectShader;
		//
		static void LoadResources();
		static void UnloadResources();
};
