#include "Maze3D.h"
#include <raylib.h>
#include "TitleScreen.h"
#include "OptionsScreen.h"
#include "GameplayScreen.h"
#include "EndingScreen.h"
#include "ResourcesManager.h"
#include "RaylibUtils.h"

using namespace std;

Maze3D::Maze3D()
{
	ResourcesManager::LoadResources();

	_activeScreen = nullptr;

	SwitchTo(TITLE);
}

void Maze3D::SwitchTo(int screen, ScreenData* data)
{
	if (_activeScreen != nullptr)
		delete _activeScreen;

	switch (screen)
	{
		case TITLE:
			_activeScreen = new TitleScreen(this, data);
			break;
		case OPTIONS:
			_activeScreen = new OptionsScreen(this, data);
			break;
		case GAMEPLAY:
			_activeScreen = new GameplayScreen(this, data);
			break;
		case ENDING:
			_activeScreen = new EndingScreen(this, data);
			break;
		default:
			break;
	}
}

void Maze3D::Update()
{
	_activeScreen->Update();
	_activeScreen->Draw();
}

Maze3D::~Maze3D()
{
	delete _activeScreen;
	ResourcesManager::UnloadResources();
}

///////////////////

int main(void)
{
	InitWindow(0, 0, "MAZE 3D!");
	
	// Compute the window position and size so it is centered and fills
	// 75% of the screen vertical space with an aspect ratio of 16:9
	int width = GetScreenWidth();
	int height = GetScreenHeight();
	float aspectRatio = 16.0f / 9.0f;
	int h = height * 0.75f;
	int w = h * aspectRatio;
	int x = width / 2 - w / 2;
	int y = height / 2 - h / 2;
	cout << x << ", " << y << ", " << w << ", " << h << "\n";
	SetWindowPosition(x, y);
	SetWindowSize(w, h);

	SetTargetFPS(60);
	InitAudioDevice();

	Maze3D maze;

	while (!WindowShouldClose())
	{
		maze.Update();
	}

	// Obsolete
	//StopSoundMulti();
	CloseAudioDevice();
	CloseWindow();
}