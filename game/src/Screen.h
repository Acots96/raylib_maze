#pragma once
#include <iostream>
#include "Actor.h"

class ScreensManager;
class ScreenData;

// Any screen in the game must inherit from this class.
// Simulates the behaviour of a FSM using a manager
class Screen : public Actor
{
	protected:
		int _width;
		int _height;
		ScreensManager* _manager;
		ScreenData* _data;

	public:
		Screen(ScreensManager* manager, ScreenData* data = nullptr)
			: Actor()
		{
			_width = GetRenderWidth();
			_height = GetRenderHeight();
			_manager = manager;
			_data = data;
		}
};