#pragma once
#include <raylib.h>
#include "RaylibUtils.h"
#include <list>

using namespace std;

// Main class from which any game object that needs translation
// and rotation must inherit considering a parent <-> childs
// structure
class Actor
{
	// Wrapper used internally to facilitate the interaction
	// with the actor's matrix.
	// (Only position implemented, not rotation)
	using TransformMatrix = struct TransformMatrix
	{
		Matrix matrix = identityM;
		Vector3 GetPosition()
		{
			return { matrix.m12, matrix.m13, matrix.m14 };
		}
		void SetPostion(Vector3 p)
		{
			matrix.m12 = p.x; matrix.m13 = p.y; matrix.m14 = p.z;
		}
	};

	private:
		Actor* _parent = nullptr;
		list<Actor*> _childs;
		TransformMatrix _localMatrix;
		TransformMatrix _worldMatrix;
		//
		void UpdateChilds();

	public:
		explicit Actor() {};
		// Override this method to do any updates needed in the
		// derived class.
		virtual void Update() {};
		// Override this method to draw anything in the derived
		// class.
		virtual void Draw() {};
		virtual ~Actor() = default;
		//
		Vector3 GetLocalPosition() { return _localMatrix.GetPosition(); }
		Vector3 GetWorldPosition() { return _worldMatrix.GetPosition(); }
		//
		void SetParent(Actor* actor);
		// Translate this actor to the specified local position
		// (world position if no parent).
		void Translate(Vector3);
		// Not implemented yet.
		void Rotate(Vector3);
		// Transform a position from this actor local space to 
		// world coordinates.
		Vector3 TransformLocalToWorld(Vector3);
		// Transform world coordinates to this actor local space.
		Vector3 TransformWorldToLocal(Vector3);
};