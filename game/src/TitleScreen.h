#pragma once
#include <iostream>
#include <raylib.h>
#include "Screen.h"
#include "ScreenData.h"

// Screen shown after the LogoScreen,
// we could say it is the main menu
class TitleScreen : public Screen
{
	private:
		Texture2D _image;
		float _imageAspectRatio;
		Rectangle _source;
		Rectangle _dest;
		//
		const char* _text;

	public:
		TitleScreen(ScreensManager*, ScreenData* = nullptr);
		void Update() override;
		void Draw() override;
		~TitleScreen() override;
};
