#include "Actor.h"

void Actor::SetParent(Actor* parent)
{ 
	if (parent == nullptr)
	{
		_parent = nullptr;
	}
	else if (parent != _parent)
	{
		// Remove this from the old parents' childs before seting the new parent.
		if (_parent != nullptr &&
			find(_parent->_childs.begin(), _parent->_childs.end(), this) != _parent->_childs.end())
			_parent->_childs.remove(this);
		_parent = parent;
		_parent->_childs.push_back(this);
	}
}

void Actor::Translate(Vector3 newLocalPosition)
{
	_localMatrix.SetPostion(newLocalPosition);
	UpdateChilds();
}

void Actor::Rotate(Vector3 anglesRotate)
{
	// Not implemented yet.
}

Vector3 Actor::TransformLocalToWorld(Vector3 local)
{
	return _worldMatrix.matrix * local;
}

Vector3 Actor::TransformWorldToLocal(Vector3 world)
{
	return Inverse(_worldMatrix.matrix) * world;
}

void Actor::UpdateChilds()
{
	// Recompute the worldmatrix checking on parent's worldmatrix.
	if (_parent == nullptr) _worldMatrix.matrix = _localMatrix.matrix;
	else _worldMatrix.matrix = _parent->_worldMatrix.matrix * _localMatrix.matrix;
	
	// Tell the children about the new position to reposition them.
	for (auto& child : _childs)
	{
		child->UpdateChilds();
	}
}