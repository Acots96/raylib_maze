#pragma once
#include <iostream>
#include <raylib.h>
#include "Screen.h"
#include "ScreenData.h"

// Screen shown when the player finishes the game.
class EndingScreen : public Screen
{
	private:
		const char* _text;
		Vector2 _textPosition;

	public:
		EndingScreen(ScreensManager*, ScreenData* = nullptr);
		void Update() override;
		void Draw() override;
		~EndingScreen() override;
};
