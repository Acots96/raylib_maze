#pragma once
#include "Actor.h"
#include <raylib.h>
#include "MazeMap.h"

class GameManager;

// Represents the player
class Player : public Actor
{
	private:
		Vector3 _position;
		Vector3 _lookAtPosition;
		Vector3 _rotation;
		float _startY;
		//
		float _turnSpeed;
		float _moveSpeed;
		float _runMultiplier;
		float _stepAmplitude;
		bool _canStepSound;
		int _stepFoot;
		bool _walkThroughWallsCheat;
		//
		Camera3D _camera;
		BoundingBox _body;
		float _bodyScale;
		MazeMap* _map;
		GameManager* _manager;
		//
		void UpdatePosition();
		void UpdateRotation();

	public:
		explicit Player() = default;
		explicit Player(GameManager* manager, MazeMap* map,
			Vector3 startPosition, float bodyScale);
		void Update() override;
		void Draw() override;
		void Reset(MazeMap* map, Vector3 startPosition);
		Camera3D GetCamera();
};
