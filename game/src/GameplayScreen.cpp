#include "GameplayScreen.h"
#include <string>
#include "Maze3D.h"
#include "ResourcesManager.h"
#include "EndingScreenData.h"

GameplayScreen::GameplayScreen(ScreensManager* manager, ScreenData* data)
	: Screen(manager, data)
{
	//_game = GameManager();
}

void GameplayScreen::Update()
{
	if (_game.GetState() != GameManager::State::Playing)
	{
		_manager->SwitchTo(Maze3D::Screens::ENDING,
			new EndingScreenData(_game.GetState() == GameManager::Win));
	}
	else _game.Update();
}

void GameplayScreen::Draw()
{
	if (_game.GetState() == GameManager::State::Playing) _game.Draw();
}

GameplayScreen::~GameplayScreen()
{
	
}
