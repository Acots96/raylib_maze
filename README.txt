UOC - Graphic Programming - Aleix Cots Molina

PEC3: Maze3D

*(source files are stored in "game" folder, resources in "resources" folder)*

Link to video (in folder "Portfolio/Raylib(C++)/Maze3D"): https://drive.google.com/drive/folders/1f19dK-otYpjN3iWk3WBaSh5HfGdSFySk?usp=sharing

===========================================================

Overview:

	- First person game where you need to complete all the mazes.
	- To complete a maze you need to find the exit door and cross it, but it is hidden and you need to find a to open it. Clue: there are collectable
	  objects all over the maze...
	- Use the keys WASD to move the character over the terrain, the mouse to move the camera and press V to trigger XVision. Take a look over the
	  Options screen to read more about how to play.
	
---------------------

Main features:
	
	- GenMeshCubicmapWithProps: This function takes an image that represents a cubic map (one pixel for cube) and each pixel's RGB channel
	  to store different information:
		- R to define whether it is transitable or not (0/255), or if it is an object (128).
		- G to define the object in this cube, being any number between 1 and 253, because 0 means no object and 254/255 are reserved for start
		  and finish positions of the maze respectively.
		- B to define the texture for this cube.
	  This function stores an object called Cubicmap, which stores all the needed information like the models of the cubic map and the objects
	  and their sizes and local positions, the obstacles positions.
	  
	- Character movement: Character's movement has been manually implemented with the needed calculations for the movement in XZ plane with
	  camera rotation and walk/run simulation using sinus function.
	
	- XVision effect: During a maze, the player is able to activate the XVision effect for a certain amount of time, which paints the screen
	  using the grayscale shader, and then uses a second camera to render only the remaining props in the maze (so they will be seen over the walls
	  like the player could see through them) and paints the with redscale shader to highlight them over the grayscale screen.
	
	- Maze start/finish effect: At the beginning and end of every maze there is a fade effect using pixelizer shader, to do the pixelized effect and
	  a smooth effect that fades the screen to black or the other way around.
	
	- Exit door effect: After collecting all the props in the maze the exit door will appear, which is an invisible cylinder that uses a shader
	  to create a "laser" effect that goes up and down.

---------------------

Classes:
		  
	- Actor: Base class that any object in the game should inherit to automatically implement features like 3D translation and rotation, or
	  parent/children hierarchy that will automatically update children when the parent is moved or rotated (though rotation is not implemented yet
	  because I did not have enough time).

	- Maze3D: Main class of the game. Works as a FSM of screens to switch between them.
	
	- Screens system:
		- ScreensManager: Interface that the manager of the screens (Paratrooper) must implement so they can ask to switch from one to another.
		- Screen: Base class that all screens must implement.
		- TitleScreen: Acts as a main menu which allows the player to switch to OptionsScreen or start the game (GameplayScreen).
		- OptionsScreen: Shows a little information about the game and how to play. Switches to TitleScreen when the player hits "O".
		- GameplayScreen: Starts the game and calls GameManager update and draw functions. Eventually, when the game is over, it will switch to
		  EndingScreen.
		- EndingScreen: Last screen of the game, shows the win message. Will allow the user to switch to TitleScreen or OptionsScreen.
		- ScreenData: Base class for any class intended to hold data for a screen.
		- EndingScreenData: Has a boolean that tells whether the player won or not, and is sent to the EndingScreen when it is created.
	
	- GameManager: Main class of the gameplay part. Instantiates the maps and the player, and makes the needed resets when the player finishes
	  a maze to put the next one. It also does the XVision effect.
	  
	- MazeMap: Holds all the data of the map (objects, obstacles etc.).
	  
	- Player: Receives the input to move the Character all over the map, to trigger XVision and checks when an object is collected and the maze
	  is finished (checking exit door).
	  
	- Prop: Contains the information of an object in the map and have methods to do the needed changes like compute its body (recalculate the box)
	  or change its shader.
	
	- RaylibUtils: File with useful methods like "GenMeshCubicmapWithProps" and overriden operator functions of Vector3 and Matrix to allow implicit
	  operations like Vector3 additions, substractions, multiplications, etc.
	
	- ResourcesManager: Static class that implements all the Load/Unload methods and all the external data for textures, audio and 3D obejcts and shaders:

---------------------
	
Raylib functions used:

	- Drawing:
		- Basic initializations/deactivations: InitWindow/CloseWindow, SetTargetFPS, LoadTexture/UnloadTexture and LoadFont/UnloadFont.
		- BeginDrawing and ClearBackground called before drawing the actors, and EndDrawing called right after.
		- BeginMode3D and EndMode3D to render cameras
		- BeginTextureMode/EndTextureMode to render what cameras "see" into a texture to render more than one camera.
		- DrawTextEx to draw a text with a certain font.
			- MeasureText to get the width of a text so it can be centered horizontally.
		
	- Sounds:
		- Basic initializations/deactivations: InitAudioDevice/CloseAudioDevice, LoadSound/UnloadSound and 
		  LoadMusicStream/UnloadMusicStream.
		- PlaySound/StopSound.
		- PlayMusicStream, SetMusicVolume, UpdateMusicStream and StopMusicStream to manage a music field.
		
	- Shaders:
	    - BeginShaderMode/EndShaderMode to allow the use of a shader when rendering a camera.
		- Load/Unload functions for shaders.
		- Set a shader in the material of a model to allow the use of it when rendering this model.
		- GetShaderLocation to store the "location" of a variable inside the shader.
		- SetShaderValue to update a variable inside a shader.
		
	- Maps:
	    - LoadImage and custom GenMeshCubicmapWithProps to load maps and compute them as Cubicmap objects.
		
	- 3D objects and textures:
		- LoadModel to load .obj files and LoadTexture.
		
	- CheckCollisionBoxes to check if two BoundingBox collide.
	- GetRayCollisionBox to get detailed information about the collision of a Ray towards a BoundingBox.
	
	- Operations with
		- Vector3: Vector3[Normalize,Add,Subtract,Negative,Scale,Divide,Equals,Transform]
		- Matrix: Matrix[Multiply,Invert]
	
---------------------

*Look over the classes and methods for further information about the implementation*

===========================================================